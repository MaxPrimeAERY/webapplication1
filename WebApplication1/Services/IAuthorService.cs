using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.API.Models.Requests.Author;
using WebApplication1.Entity.Entities;

namespace WebApplication1.Services
{
    public interface IAuthorService
    {
        Task<Author> GetById(int id);
        Task<IList<Author>> GetAll();
        Task Delete(int id);
        Task Update(int authorId, string valueText);
        Task<Author> Create(CreateAuthor value);
    }
}