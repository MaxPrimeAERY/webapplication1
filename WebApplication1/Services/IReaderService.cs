using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.API.Models.Requests.Reader;
using WebApplication1.Entity.Entities;

namespace WebApplication1.Services
{
    public interface IReaderService
    {
        Task<Reader> GetById(int id);
        Task<IList<Reader>> GetAll();
        Task Delete(int id);
        Task Update(int readerId, string valueText);
        Task<Reader> Create(CreateReader value);
    }
}