using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.API.Models.Requests.Sections;
using WebApplication1.Entity.Entities;

namespace WebApplication1.Services
{
    public interface ISectionsService
    {
        Task<Sections> GetById(int id);
        Task<IList<Sections>> GetAll();
        Task Delete(int id);
        Task Update(int sectionsId, List<string> valueLists);
        Task<Sections> Create(CreateSections value);
    }
}