using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.API.Models.Requests.CardOfReader;
using WebApplication1.Entity.Entities;

namespace WebApplication1.Services
{
    public interface ICardOfReaderService
    {
        Task<CardOfReader> GetById(int id);
        Task<IList<CardOfReader>> GetAll();
        Task<IList<CardOfReader>> GetByReaderId(int id);
        Task<IList<CardOfReader>> GetByBookId(int id);
        Task Delete(int id);
        Task Update(int corId, DateTime valueGiveTimeOffset);
        Task<CardOfReader> Create(CreateCardOfReader value);
    }
}