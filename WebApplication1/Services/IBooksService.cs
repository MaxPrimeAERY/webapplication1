using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.API.Models.Requests.Books;
using WebApplication1.Entity.Entities;

namespace WebApplication1.Services
{
    public interface IBooksService
    {
        Task<Books> GetById(int id);
        Task<IList<Books>> GetAll();
        Task<IList<Books>> GetBySectionsId(int id);
        Task Delete(int id);
        Task Update(int bookId, string bookName, string genre, int sectionsId);
        Task<Books> Create(CreateBooks value);
    }
}