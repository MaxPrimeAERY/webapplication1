using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.API.Models.Requests.BookOfAuthor;
using WebApplication1.Entity.Entities;

namespace WebApplication1.Services
{
    public interface IBookOfAuthorService
    {
        Task<BookOfAuthor> GetById(int id);
        Task<IList<BookOfAuthor>> GetAll();
        Task<IList<BookOfAuthor>> GetByAuthorId(int id);
        Task<IList<BookOfAuthor>> GetByBookId(int id);
        Task Delete(int id);
        Task Update(int bookofauthorId, int? valueAuthorId, int? valueBookId);
        Task<BookOfAuthor> Create(CreateBookOfAuthor value);
    }
}