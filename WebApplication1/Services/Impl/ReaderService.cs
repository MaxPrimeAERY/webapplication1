using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.API.Models.Requests.Reader;
using WebApplication1.DAL.Abstract;
using WebApplication1.Entity.Entities;

namespace WebApplication1.Services.Impl
{
    public class ReaderService : IReaderService
    {
        private IReaderRepository _readerRepo;

        public ReaderService(IReaderRepository readerRepo)
        {
            _readerRepo = readerRepo;

        }
        public Task<Reader> GetById(int id)
        {
            return _readerRepo.GetById(id);
        }

        public Task<IList<Reader>> GetAll()
        {
            return _readerRepo.GetAll();
        }

        public async Task Delete(int id)
        {
            var res = await _readerRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public  async Task<Reader> Create(CreateReader value)
        {
            var author = new Reader()
            {
                Name = value.Name
            };

            var id = await _readerRepo.Insert(author);
            author.Id = id;
            return author;
        }

        public async Task Update(int authId, string valueName)
        {
            var reader = await _readerRepo.GetById(authId);
            reader.Name = valueName;
            await _readerRepo.Update(reader);
        }
    }
}