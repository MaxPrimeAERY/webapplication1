using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.API.Models.Requests.Author;
using WebApplication1.DAL.Abstract;
using WebApplication1.Entity.Entities;

namespace WebApplication1.Services.Impl
{
    public class AuthorService : IAuthorService
    {
        private IAuthorRepository _authRepo;

        public AuthorService(IAuthorRepository authRepo)
        {
            _authRepo = authRepo;

        }
        public Task<Author> GetById(int id)
        {
            return _authRepo.GetById(id);
        }

        public Task<IList<Author>> GetAll()
        {
            return _authRepo.GetAll();
        }

        public async Task Delete(int id)
        {
            var res = await _authRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public  async Task<Author> Create(CreateAuthor value)
        {
            var author = new Author()
            {
                Name = value.Name
            };

            var id = await _authRepo.Insert(author);
            author.Id = id;
            return author;
        }

        public async Task Update(int authId, string valueName)
        {
            var author = await _authRepo.GetById(authId);
            author.Name = valueName;
            await _authRepo.Update(author);
        }

       
    }
}