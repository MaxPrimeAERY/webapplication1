using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.API.Models.Requests.BookOfAuthor;
using WebApplication1.DAL.Abstract;
using WebApplication1.Entity.Entities;

namespace WebApplication1.Services.Impl
{
    public class BookOfAuthorService : IBookOfAuthorService
    {
        private IBookOfAuthorRepository _bookOfAuthorRepo;

        public BookOfAuthorService(IBookOfAuthorRepository bookOfAuthorRepo)
        {
            _bookOfAuthorRepo = bookOfAuthorRepo;

        }

        public Task<BookOfAuthor> GetById(int id)
        {
            return _bookOfAuthorRepo.GetById(id);
        }

        public Task<IList<BookOfAuthor>> GetAll()
        {
            return _bookOfAuthorRepo.GetAll();
        }
        
        public Task<IList<BookOfAuthor>> GetByAuthorId(int id)
        {
            return _bookOfAuthorRepo.GetByAuthorId(id);
        }
        
        public Task<IList<BookOfAuthor>> GetByBookId(int id)
        {
            return _bookOfAuthorRepo.GetByBookId(id);
        }
        
        public async Task Delete(int id)
        {
            var res = await _bookOfAuthorRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }

        public  async Task<BookOfAuthor> Create(CreateBookOfAuthor value)
        {
            var bookofauthor = new BookOfAuthor()
            {
                AuthorId = value.AuthorId,
                BookId = value.BookId
            };

            var id = await _bookOfAuthorRepo.Insert(bookofauthor);
            bookofauthor.Id = id;
            return bookofauthor;
        }
        
        public async Task Update(int bookofauthorId, int? valueAuthorId, int? valueBookId)
        {
            var bookofauthor = await _bookOfAuthorRepo.GetById(bookofauthorId);
            bookofauthor.AuthorId = valueAuthorId;
            bookofauthor.BookId = valueBookId;
            await _bookOfAuthorRepo.Update(bookofauthor);
        }

    }
}