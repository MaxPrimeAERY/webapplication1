using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.Entity.Entities;

namespace WebApplication1.DAL.Abstract
{
    public interface ICardOfReaderRepository : IBaseRepository<int, CardOfReader>
    {
        Task<IList<CardOfReader>> GetByReaderId(int readerId);
        Task<IList<CardOfReader>> GetByBookId(int bookId);
    }
}