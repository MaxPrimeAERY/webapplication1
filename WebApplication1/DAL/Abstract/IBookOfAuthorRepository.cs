using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.Entity.Entities;

namespace WebApplication1.DAL.Abstract
{
    public interface IBookOfAuthorRepository : IBaseRepository<int, BookOfAuthor>
    {
        Task<IList<BookOfAuthor>> GetByAuthorId(int fullId);
        
        Task<IList<BookOfAuthor>> GetByBookId(int fullId);
    }
}