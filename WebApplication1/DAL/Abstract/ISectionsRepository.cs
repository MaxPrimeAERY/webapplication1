using WebApplication1.Entity.Entities;

namespace WebApplication1.DAL.Abstract
{
    public interface ISectionsRepository : IBaseRepository<int, Sections>
    {
        
    }
}