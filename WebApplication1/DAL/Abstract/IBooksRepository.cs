using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication1.Entity.Entities;

namespace WebApplication1.DAL.Abstract
{
    public interface IBooksRepository : IBaseRepository<int, Books>
    {
        Task<IList<Books>> GetBySectionsId(int sectId);
    }
}