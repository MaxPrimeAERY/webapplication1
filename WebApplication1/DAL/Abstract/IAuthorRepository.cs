using WebApplication1.Entity.Entities;

namespace WebApplication1.DAL.Abstract
{
    public interface IAuthorRepository  : IBaseRepository<int, Author>
    {
        
    }
}