using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using WebApplication1.DAL.Abstract;
using WebApplication1.DAL.Pgsql.Infrastructure;
using WebApplication1.Entity.Entities;

namespace WebApplication1.DAL.Pgsql.Repository
{
    internal class ReaderRepository : BaseRepository<int, Reader>, IReaderRepository
    {
        public ReaderRepository(DbConnection connection, ITransactionManager transactionManager) : base(connection,
            transactionManager)
        {
        }

        protected override Reader DefaultRowMapping(DbDataReader reader)
        {
            return new Reader
            {
                Id = (int) reader["id"],
                Name = reader["name"] as string,
            };
        }

        public override Task<int> Insert(Reader entity)
        {
            return base.ExecuteScalar<int>(
                "insert into \"Reader\" (name) values (@Name) RETURNING id",
                new SqlParameters
                {
                    {"Name", entity.Name},
                }
            );
        }

        public override async Task<bool> Update(Reader entity)
        {
            var res = await base.ExecuteNonQuery(
                "update \"Reader\" set name = @Name  where id = @Id ",
                new SqlParameters
                {
                    {"Name", entity.Name},
                    {"Id", entity.Id},
                }
            );

            return res > 0;
        }

        public Task<int> GetCount()
        {
            return base.ExecuteScalar<int>("select count(*) from \"Reader\"");
        }

        public Task<Reader> GetById(int id)
        {
            return base.ExecuteSingleRowSelect(
                "select a.id, a.name from \"Reader\" a where a.id = @Id",
                new SqlParameters()
                {
                    {"Id", id}
                }
            );
        }

        public async Task<bool> Delete(int id)
        {
            var res = await base.ExecuteNonQuery(
                "delete from \"Reader\" where id = @id",
                new SqlParameters() {{"id", id}});

            if (res > 1)
                throw new InvalidOperationException("Multiple rows deleted by single delete query");

            return res == 1;
        }

        public Task<IList<Reader>> GetAll()
        {
            return base.ExecuteSelect("select a.id, a.name from \"Reader\" a");
        }
    }
}