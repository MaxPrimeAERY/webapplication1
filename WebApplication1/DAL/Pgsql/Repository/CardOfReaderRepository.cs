using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using WebApplication1.DAL.Abstract;
using WebApplication1.DAL.Pgsql.Infrastructure;
using WebApplication1.Entity.Entities;

namespace WebApplication1.DAL.Pgsql.Repository
{
    internal class CardOfReaderRepository : BaseRepository<int, CardOfReader>, ICardOfReaderRepository
    {
        public CardOfReaderRepository(DbConnection connection, ITransactionManager transactionManager) : base(connection,
            transactionManager)
        {
        }

        protected override CardOfReader DefaultRowMapping(DbDataReader reader)
        {
            return new CardOfReader
            {
                Id = (int) reader["id"],
                Reader_Id = (int) reader["reader_id"],
                Book_Id = (int) reader["book_id"],
                Get_Time = reader["get_time"] as DateTime? ?? default,
                Give_Time = reader["give_time"] as DateTime? ?? default
            };
        }

        public override Task<int> Insert(CardOfReader entity)
        {
            return base.ExecuteScalar<int>(
                "insert into \"CardOfReader\" (reader_id, book_id, get_time) values (@Reader_Id, @Book_Id, @Get_Time) RETURNING id",
                new SqlParameters
                {
                    {"Reader_Id", entity.Reader_Id},
                    {"Book_Id", entity.Book_Id},
                    {"Get_Time", entity.Get_Time},
                }
            );
        }

        public override async Task<bool> Update(CardOfReader entity)
        {
            var res = await base.ExecuteNonQuery(
                "update \"CardOfReader\" set give_time = @Give_Time  where id = @Id ",
                new SqlParameters
                {
                    {"Id", entity.Id},
                    {"Give_Time", entity.Give_Time},
                }
            );

            return res > 0;
        }

        public Task<int> GetCount()
        {
            return base.ExecuteScalar<int>("select count(*) from \"CardOfReader\"");
        }

        public Task<CardOfReader> GetById(int id)
        {
            return base.ExecuteSingleRowSelect(
                "select c.id, reader_id, book_id, get_time, give_time from \"CardOfReader\" c where c.id = @Id",
                new SqlParameters()
                {
                    {"Id", id}
                }
            );
        }

        public async Task<bool> Delete(int id)
        {
            var res = await base.ExecuteNonQuery(
                "delete from \"CardOfReader\" where id = @id",
                new SqlParameters() {{"id", id}});

            if (res > 1)
                throw new InvalidOperationException("Multiple rows deleted by single delete query");

            return res == 1;
        }

        public Task<IList<CardOfReader>> GetAll()
        {
            return base.ExecuteSelect("select c.id, c.reader_id, c.book_id, c.get_time, c.give_time from \"CardOfReader\" c");
        }

        public Task<IList<CardOfReader>> GetByReaderId(int reader_id)
        {
            return base.ExecuteSelect("Select id, reader_id, book_id, give_time, get_time from \"CardOfReader\" WHERE reader_id = @Reader_Id",
                new SqlParameters()
                {
                    {"Reader_Id", reader_id}
                });
        }

        public Task<IList<CardOfReader>> GetByBookId(int book_id)
        {
            return base.ExecuteSelect("Select id, reader_id, book_id, give_time, get_time from \"CardOfReader\" WHERE book_id = @Book_Id",
                new SqlParameters()
                {
                    {"Book_Id", book_id}
                });
        }
    }
}