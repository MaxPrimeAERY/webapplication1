using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using WebApplication1.DAL.Abstract;
using WebApplication1.DAL.Pgsql.Infrastructure;
using WebApplication1.Entity.Entities;

namespace WebApplication1.DAL.Pgsql.Repository
{
    internal class BookOfAuthorRepository : BaseRepository<int, BookOfAuthor>, IBookOfAuthorRepository
    {
        public BookOfAuthorRepository(DbConnection connection, ITransactionManager transactionManager) : base(connection,
            transactionManager)
        {
        }

        protected override BookOfAuthor DefaultRowMapping(DbDataReader reader)
        {
            return new BookOfAuthor
            {
                Id = (int) reader["id"],
                AuthorId = (int) reader["author_id"],
                BookId = (int) reader["book_id"] 
            };
        }

        public override Task<int> Insert(BookOfAuthor entity)
        {
            return base.ExecuteScalar<int>(
                "insert into \"BookOfAuthor\" (author_id, book_id) values (@Author_Id, @Book_Id) RETURNING id",
                new SqlParameters
                {
                    {"Author_Id", entity.AuthorId},
                    {"Book_Id", entity.BookId},
                }
            );
        }

        public override async Task<bool> Update(BookOfAuthor entity)
        {
            var res = await base.ExecuteNonQuery(
                "update \"BookOfAuthor\" set author_id = @Author_id, book_id = @Book_id  where id = @Id ",
                new SqlParameters
                {
                    {"Id", entity.Id},
                    {"Author_id", entity.AuthorId},
                    {"Book_id", entity.BookId},
                }
            );

            return res > 0;
        }

        public Task<int> GetCount()
        {
            return base.ExecuteScalar<int>("select count(*) from \"BookOfAuthor\"");
        }

        public Task<BookOfAuthor> GetById(int id)
        {
            return base.ExecuteSingleRowSelect(
                "select b.id, b.author_id, b.book_id from \"BookOfAuthor\" b where b.id = @Id",
                new SqlParameters()
                {
                    {"Id", id}
                }
            );
        }

        public async Task<bool> Delete(int id)
        {
            var res = await base.ExecuteNonQuery(
                "delete from \"BookOfAuthor\" where id = @id",
                new SqlParameters() {{"id", id}});

            if (res > 1)
                throw new InvalidOperationException("Multiple rows deleted by single delete query");

            return res == 1;
        }

        public Task<IList<BookOfAuthor>> GetAll()
        {
            return base.ExecuteSelect("select b.id, b.author_id, b.book_id from \"BookOfAuthor\" b");
        }

        public Task<IList<BookOfAuthor>> GetByAuthorId(int fullId)
        {
            return base.ExecuteSelect("Select id, author_id, book_id from \"BookOfAuthor\" WHERE author_id = @author_id",
                new SqlParameters()
                {
                    {"author_id", fullId}
                });
        }

        public Task<IList<BookOfAuthor>> GetByBookId(int fullId)
        {
            return base.ExecuteSelect("Select id, author_id, book_id from \"BookOfAuthor\" WHERE book_id = @book_id",
                new SqlParameters()
                {
                    {"book_id", fullId}
                });
        }
    }
}