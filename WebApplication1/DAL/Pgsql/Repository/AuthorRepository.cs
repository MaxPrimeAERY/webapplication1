using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using WebApplication1.DAL.Abstract;
using WebApplication1.DAL.Pgsql.Infrastructure;
using WebApplication1.Entity.Entities;

namespace WebApplication1.DAL.Pgsql.Repository
{
    internal class AuthorRepository : BaseRepository<int, Author>, IAuthorRepository
    {
        public AuthorRepository(DbConnection connection, ITransactionManager transactionManager) : base(connection,
            transactionManager)
        {
        }

        protected override Author DefaultRowMapping(DbDataReader reader)
        {
            return new Author
            {
                Id = (int) reader["id"],
                Name = reader["name"] as string,
            };
        }

        public override Task<int> Insert(Author entity)
        {
            return base.ExecuteScalar<int>(
                "insert into \"Author\" (name) values (@Name) RETURNING id",
                new SqlParameters
                {
                    {"Name", entity.Name},
                }
            );
        }

        public override async Task<bool> Update(Author entity)
        {
            var res = await base.ExecuteNonQuery(
                "update \"Author\" set name = @Name  where id = @Id ",
                new SqlParameters
                {
                    {"Name", entity.Name},
                    {"Id", entity.Id},
                }
            );

            return res > 0;
        }

        public Task<int> GetCount()
        {
            return base.ExecuteScalar<int>("select count(*) from \"Author\"");
        }

        public Task<Author> GetById(int id)
        {
            return base.ExecuteSingleRowSelect(
                "select a.id, a.name from \"Author\" a where a.id = @Id",
                new SqlParameters()
                {
                    {"Id", id}
                }
            );
        }

        public async Task<bool> Delete(int id)
        {
            var res = await base.ExecuteNonQuery(
                "delete from \"Author\" where id = @id",
                new SqlParameters() {{"id", id}});

            if (res > 1)
                throw new InvalidOperationException("Multiple rows deleted by single delete query");

            return res == 1;
        }

        public Task<IList<Author>> GetAll()
        {
            return base.ExecuteSelect("select a.id, a.name from \"Author\" a");
        }
    }
}