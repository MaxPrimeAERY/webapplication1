using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using WebApplication1.DAL.Abstract;
using WebApplication1.DAL.Pgsql.Infrastructure;
using WebApplication1.Entity.Entities;

namespace WebApplication1.DAL.Pgsql.Repository
{
    internal class SectionsRepository : BaseRepository<int, Sections>, ISectionsRepository
    {
        public SectionsRepository(DbConnection connection, ITransactionManager transactionManager) : base(connection,
            transactionManager)
        {
        }

        protected override Sections DefaultRowMapping(DbDataReader reader)
        {
            return new Sections
            {
                Id = (int) reader["id"],
                SectionsNames = new List<string>(reader["sections_names"] as string[]),
            };
        }

        public override Task<int> Insert(Sections entity)
        {
            return base.ExecuteScalar<int>(
                "insert into \"Sections\" (sections_names) values (@sections_names) RETURNING id",
                new SqlParameters
                {
                    {"Sections_Names", entity.SectionsNames},
                }
            );
        }

        public override async Task<bool> Update(Sections entity)
        {
            var res = await base.ExecuteNonQuery(
                "update \"Sections\" set sections_names = @sections_names  where id = @Id ",
                new SqlParameters
                {
                    {"Sections_Names", entity.SectionsNames},
                    {"Id", entity.Id},
                }
            );

            return res > 0;
        }

        public Task<int> GetCount()
        {
            return base.ExecuteScalar<int>("select count(*) from \"Sections\"");
        }

        public Task<Sections> GetById(int id)
        {
            return base.ExecuteSingleRowSelect(
                "select a.id, a.sections_names from \"Sections\" a where a.id = @Id",
                new SqlParameters()
                {
                    {"Id", id}
                }
            );
        }

        public async Task<bool> Delete(int id)
        {
            var res = await base.ExecuteNonQuery(
                "delete from \"Sections\" where id = @id",
                new SqlParameters() {{"id", id}});

            if (res > 1)
                throw new InvalidOperationException("Multiple rows deleted by single delete query");

            return res == 1;
        }

        public Task<IList<Sections>> GetAll()
        {
            return base.ExecuteSelect("select a.id, a.sections_names from \"Sections\" a");
        }
    }
}