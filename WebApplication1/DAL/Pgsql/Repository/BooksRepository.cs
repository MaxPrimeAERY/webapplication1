using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using WebApplication1.DAL.Abstract;
using WebApplication1.DAL.Pgsql.Infrastructure;
using WebApplication1.Entity.Entities;

namespace WebApplication1.DAL.Pgsql.Repository
{
    internal class BooksRepository : BaseRepository<int, Books>, IBooksRepository
    {
        public BooksRepository(DbConnection connection, ITransactionManager transactionManager) : base(connection,
            transactionManager)
        {
        }

        protected override Books DefaultRowMapping(DbDataReader reader)
        {
            return new Books
            {
                Id = (int) reader["id"],
                BookName = reader["bookname"] as string,
                Genre = reader["genre"] as string,
                Sections_Id = (int) reader["sections_id"]
            };
        }

        public override Task<int> Insert(Books entity)
        {
            return base.ExecuteScalar<int>(
                "insert into \"Books\" (bookname, genre, sections_id) values (@BookName, @Genre, @Sections_id) RETURNING id",
                new SqlParameters
                {
                    {"BookName", entity.BookName},
                    {"Genre", entity.Genre},
                    {"Sections_Id", entity.Sections_Id},
                }
            );
        }

        public override async Task<bool> Update(Books entity)
        {
            var res = await base.ExecuteNonQuery(
                "update \"Books\" set bookname = @BookName, genre = @Genre, sections_id = @Sections_id  where id = @Id ",
                new SqlParameters
                {
                    {"Id", entity.Id},
                    {"BookName", entity.BookName},
                    {"Genre", entity.Genre},
                    {"Sections_Id", entity.Sections_Id},
                    }
            );

            return res > 0;
        }

        public Task<int> GetCount()
        {
            return base.ExecuteScalar<int>("select count(*) from \"Books\"");
        }

        public Task<Books> GetById(int id)
        {
            return base.ExecuteSingleRowSelect(
                "select b.id, b.bookname, b.genre, b.sections_id from \"Books\" b where b.id = @Id",
                new SqlParameters()
                {
                    {"Id", id}
                }
            );
        }

        public async Task<bool> Delete(int id)
        {
            var res = await base.ExecuteNonQuery(
                "delete from \"Books\" where id = @id",
                new SqlParameters() {{"id", id}});

            if (res > 1)
                throw new InvalidOperationException("Multiple rows deleted by single delete query");

            return res == 1;
        }

        public Task<IList<Books>> GetAll()
        {
            return base.ExecuteSelect("select b.id, b.bookname, b.genre, b.sections_id from \"Books\" b");
        }
        
        public Task<IList<Books>> GetBySectionsId(int sectId)
        {
            return base.ExecuteSelect("Select id, bookname, genre, sections_id  from \"Books\" WHERE sections_id = @sections_Id",
                new SqlParameters()
                {
                    {"sections_Id", sectId}
                });
        }
    }
}