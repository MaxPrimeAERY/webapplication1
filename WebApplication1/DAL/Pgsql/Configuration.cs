using System.Data.Common;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;
using WebApplication1.DAL.Abstract;
using WebApplication1.DAL.Pgsql.Infrastructure;
using WebApplication1.DAL.Pgsql.Repository;

namespace WebApplication1.DAL.Pgsql
{
    public static class Configuration
    {
        public static IServiceCollection UsePgSqlAdoRepositories(this IServiceCollection services,
            string connectionString)
        {
            //we ned concrete implementation for transaction manager!
            services.AddScoped(ctx => new NpgsqlConnection(connectionString));
            services.AddScoped<DbConnection>(ctx => ctx.GetService<NpgsqlConnection>());

            services.AddScoped<ITransactionManager, PgSqlTransactionManager>();

            services.AddScoped<IAuthorRepository, AuthorRepository>();
            services.AddScoped<IBookOfAuthorRepository, BookOfAuthorRepository>();
            services.AddScoped<IBooksRepository, BooksRepository>();
            services.AddScoped<ICardOfReaderRepository, CardOfReaderRepository>();
            services.AddScoped<IReaderRepository, ReaderRepository>();
            services.AddScoped<ISectionsRepository, SectionsRepository>();

            return services;
        }
    }
}