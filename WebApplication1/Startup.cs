﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplication1.DAL.Pgsql;
using WebApplication1.Services;
using WebApplication1.Services.Impl;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // https://www.npgsql.org/doc/connection-string-parameters.html
            var connectionString = Configuration.GetConnectionString("Default");
            services.UsePgSqlAdoRepositories(connectionString);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddTransient<IAuthorService, AuthorService>();
            services.AddTransient<IBookOfAuthorService, BookOfAuthorService>();
            services.AddTransient<IBooksService, BooksService>();
            services.AddTransient<ICardOfReaderService, CardOfReaderService>();
            services.AddTransient<IReaderService, ReaderService>();
            services.AddTransient<ISectionsService, SectionsService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
//            app.UseStaticFiles();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMvc();

        }
    }
}