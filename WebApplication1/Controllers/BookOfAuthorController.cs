using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.API.Models.Requests.BookOfAuthor;
using WebApplication1.Entity.Entities;
using WebApplication1.MvcExt;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookOfAuthorController : ControllerBase
    {
        private readonly IBookOfAuthorService _bookOfAuthorSvc;

        public BookOfAuthorController(IBookOfAuthorService bookOfAuthorSvc)
        {
            _bookOfAuthorSvc = bookOfAuthorSvc;
        }
        
        [HttpGet]
        public async Task<ActionResult<IList<BookOfAuthor>>> Get()
        {
            return new ActionResult<IList<BookOfAuthor>>(await _bookOfAuthorSvc.GetAll());
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<BookOfAuthor>> Get(int id)
        {
            var bookofauthor = await _bookOfAuthorSvc.GetById(id);
            if (bookofauthor == null)
            {
                return new NotFoundObjectResult(null);
            }

            return bookofauthor;
        }

        [HttpGet]
        [ExactQueryParam("authorId")]
        public async Task<ActionResult<IEnumerable<BookOfAuthor>>> GetByAuthorId(int authorId)
        {
            return new ActionResult<IEnumerable<BookOfAuthor>>(await _bookOfAuthorSvc.GetByAuthorId(authorId));
        }
        
        [HttpGet]
        [ExactQueryParam("bookId")]
        public async Task<ActionResult<IEnumerable<BookOfAuthor>>> GetByBookId(int bookId)
        {
            return new ActionResult<IEnumerable<BookOfAuthor>>(await _bookOfAuthorSvc.GetByBookId(bookId));
        }
        
        [HttpPost]
        public async Task<ActionResult<BookOfAuthor>> Post([FromBody] CreateBookOfAuthor value)
        {
            return await _bookOfAuthorSvc.Create(value);
        }

        // PUT api/author/5
        [HttpPut("{id}")]
        public async Task<ActionResult<BookOfAuthor>> Put(int id, [FromBody] EditBookOfAuthor value)
        {
            
            await _bookOfAuthorSvc.Update(id, value.AuthorId, value.BookId);
            return await _bookOfAuthorSvc.GetById(id);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _bookOfAuthorSvc.Delete(id);
        }

    }
}