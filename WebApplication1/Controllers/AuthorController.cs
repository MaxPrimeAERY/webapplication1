using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.API.Models.Requests.Author;
using WebApplication1.Entity.Entities;
using WebApplication1.MvcExt;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorSvc;

        public AuthorController(IAuthorService authorSvc)
        {
            _authorSvc = authorSvc;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IList<Author>>> Get()
        {
            return new ActionResult<IList<Author>>(await _authorSvc.GetAll());
        }
        
        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Author>> Get(int id)
        {
            var author = await _authorSvc.GetById(id);
            if (author == null)
            {
                return new NotFoundObjectResult(null);
            }

            return author;
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<Author>> Post([FromBody] CreateAuthor value)
        {
            return await _authorSvc.Create(value);
        }

        // PUT api/author/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Author>> Put(int id, [FromBody] EditAuthor value)
        {
            
            await _authorSvc.Update(id, value.Name);
            return await _authorSvc.GetById(id);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _authorSvc.Delete(id);
        }
    }
}