using System;
using System.Collections.Generic;
using WebApplication1.Entity.Entities.Abstract;

namespace WebApplication1.Entity.Entities
{
    public class CardOfReader:IBaseEntity<int>
    {
        public int Id { get; set; }
        public int Reader_Id { get; set; }
        public int Book_Id { get; set; }
        public DateTime Get_Time { get; set; }
        public DateTime Give_Time { get; set; }
    }
}