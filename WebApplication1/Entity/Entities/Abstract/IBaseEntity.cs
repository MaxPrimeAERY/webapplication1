namespace WebApplication1.Entity.Entities.Abstract
{
    public interface IBaseEntity<TKey>
    {
        TKey Id { get; }
    }
}