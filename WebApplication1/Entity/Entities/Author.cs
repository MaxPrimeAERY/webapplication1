using System.Collections.Generic;
using WebApplication1.Entity.Entities.Abstract;

namespace WebApplication1.Entity.Entities

{
    public class Author:IBaseEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}