using System.Collections.Generic;
using WebApplication1.Entity.Entities.Abstract;

namespace WebApplication1.Entity.Entities
{
    public class Sections:IBaseEntity<int>
    {
        public int Id { get; set; }
        public List<string> SectionsNames { get; set; }
    }
}