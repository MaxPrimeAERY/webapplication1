using System.Collections.Generic;
using WebApplication1.Entity.Entities.Abstract;

namespace WebApplication1.Entity.Entities
{
    public class Books:IBaseEntity<int>
    {
        public int Id { get; set; }
        public string BookName { get; set; }
        public string Genre { get; set; }
        public int Sections_Id { get; set; }
    }
}