using System.Collections.Generic;
using WebApplication1.Entity.Entities.Abstract;

namespace WebApplication1.Entity.Entities
{
    public class BookOfAuthor:IBaseEntity<int>
    {
        public int Id { get; set; }
        public int? AuthorId { get; set; }
        public int? BookId { get; set; }
        
    }
}